# reflow
coordinate your multi-language projects with ease

## why
multi-language projects are hard. running multiple build tools is annoying. build systems like bazel and buck can't handle everything. reflow solves this by wrapping all of your exising build tools in a relatively simple manner.

## how
### installing
`npm i reflow-build -g`. if you are on arch, `reflow` is available in the aur.

### configuring
the provided example should be enough to get you going, however, it is not a great example. reflow is meant to call other build tools, not to directly invoke compilers.

### running
`reflow --help` should explain what you can do pretty well.
