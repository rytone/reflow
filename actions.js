const chalk = require("chalk");
const fs = require("fs-extra");
const path = require("path");
const { exec, spawn } = require("child-process-promise");
const { DepGraph } = require("dependency-graph");

exports.verifyTools = (project) => {
	console.log(chalk.green.bold("Checking for tools..."));
	return Promise.all(project.tools.map((tool) => {
		return exec(tool.test)
			.then(() => {
				console.log(`  Found ${tool.name}.`);
				return true;
			}).catch(() => {
				console.log(chalk.red(`  Could not find ${tool.name}.`));
				return false;
			});
	})).then((results) => {
		return results.reduce((acc, v) => {
			return acc && v;
		}, true);
	}).then((ok) => {
		if (!ok) throw "Some tools were not found.";
		return ok;
	});
}

exports.buildGraph = (project) => {
	console.log(chalk.green.bold("Building project dependency graph..."));
	let graph = new DepGraph();
	project.stages.forEach((stage) => {
		graph.addNode(stage.name, stage.command);
	});
	project.stages.forEach((stage) => {
		if (!stage.deps) return;
		stage.deps.forEach((dep) => {
			graph.addDependency(stage.name, dep);
		})
	});
	return graph;
}

const build = (graph, n) => {
	console.log(chalk.bold(`  Executing stage [${n}]...`));
	const cmd = graph.getNodeData(n);
	const cmdArray = cmd instanceof Array ? cmd : [cmd];
	return cmdPromise = cmdArray.reduce((acc, c) => {
		const thisPromise = spawn(c.bin, c.args ? c.args : [], c.dir ? {cwd: c.dir} : {});
		thisPromise.childProcess.stdout.on("data", (d) => {
			process.stdout.write(d);
		});
		thisPromise.childProcess.stderr.on("data", (d) => {
			process.stderr.write(d);
		});
		return acc.then(() => thisPromise).then(() => {
			if (c.artifacts) {
				console.log(chalk.bold("Copying artifacts..."));
				return Promise.all(c.artifacts.map((atf) => {
					return fs.copy(atf, "artifacts/" + path.basename(atf));
				}));
			}
		});
	}, Promise.resolve())
}

exports.buildAll = (graph) => {
	console.log(chalk.green.bold("Building project..."));
	return fs.ensureDir("./artifacts").then(() => {
		return graph.overallOrder().reduce((acc, n) => {
			return acc.then(() => build(graph, n));
		}, Promise.resolve());
	});
}

exports.build = (graph, stage, only) => {
	console.log(chalk.green.bold("Building project..."));
	return fs.ensureDir("./artifacts").then(() => {
		if (only) {
			return build(graph, stage);
		}
		const stages = graph.dependenciesOf(stage);
		stages.push(stage);
		return stages.reduce((acc, n) => {
			return acc.then(() => build(graph, n));
		}, Promise.resolve());
	});
}

exports.clean = () => {
	console.log(chalk.green.bold("Cleaning..."));
	return fs.remove("./artifacts");
}
