const chalk = require("chalk");

const actions = require("../actions.js");

exports.command = "build"
exports.aliases = ["build", "b"]
exports.describe = "builds the entire project"
exports.builder = {}

exports.handler = (args) => {
	const st = process.hrtime();
	return actions.verifyTools(args.project)
		.then(() => actions.buildGraph(args.project))
		.then((graph) => actions.buildAll(graph))
		.catch((e) => {
			console.log(chalk.red.bold(`Build failed: ${e}`));
		})
		.then(() => {
			const et = process.hrtime(st);
			console.log(chalk.green(`Completed in ${et[0]}s, ${(et[1]/1000000).toFixed(2)}ms`));
		});
}
