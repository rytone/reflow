const chalk = require("chalk");

const actions = require("../actions.js");

exports.command = "clean"
exports.aliases = ["clean", "c"]
exports.describe = "remove the artifacts directory"
exports.builder = {}

exports.handler = (args) => {
	const st = process.hrtime();
	return actions.clean()
		.then(() => {
			const et = process.hrtime(st);
			console.log(chalk.green(`Completed in ${et[0]}s, ${(et[1]/1000000).toFixed(2)}ms`));
		});
}
