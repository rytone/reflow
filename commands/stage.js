const chalk = require("chalk");

const actions = require("../actions.js");

exports.command = "stage <name>"
exports.aliases = ["stage", "s"]
exports.describe = "build a specific stage of your project"
exports.builder = (yargs) => yargs.option("only", {
	alias: "o",
	default: "false",
	describe: "skip building dependencies",
	type: "boolean",
});

exports.handler = (args) => {
	const st = process.hrtime();
	return actions.verifyTools(args.project)
		.then(() => actions.buildGraph(args.project))
		.then((graph) => actions.build(graph, args.name, args.only))
		.catch((e) => {
			console.log(chalk.red.bold(`Build failed: ${e}`));
		})
		.then(() => {
			const et = process.hrtime(st);
			console.log(chalk.green(`Completed in ${et[0]}s, ${(et[1]/1000000).toFixed(2)}ms`));
		});
}
