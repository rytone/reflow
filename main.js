#!/usr/bin/env node

const fs = require("fs");
const chalk = require("chalk");

if (!fs.existsSync("./reflow.json")) {
	console.log(chalk.red.bold("reflow.json not found!"));
	console.log("Please create it or move to a directory that contains it.");
	return;
}

const config = JSON.parse(fs.readFileSync("./reflow.json"))

const args = require("yargs")
	.config({ "project": config })
	.commandDir("commands")
	.demandCommand(1, "You must specify a command to continue.")
	.help()
	.argv;
